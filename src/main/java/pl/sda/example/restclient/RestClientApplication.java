package pl.sda.example.restclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.sda.example.restclient.http.client.model.Todo;
import pl.sda.example.restclient.service.ToDoService;

@SpringBootApplication
public class RestClientApplication implements CommandLineRunner {


	@Autowired
	private ToDoService toDoService;

	public static void main(String[] args) {
		SpringApplication.run(RestClientApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Todo toDo = toDoService.getToDo(1);

		Todo todoToCreate = new Todo(1, "title1", false);
		toDoService.addTodo(todoToCreate);

		Todo todoToUpdate = new Todo();
		toDoService.updateTodo(todoToUpdate);

		toDoService.getTodos();
		}
}
