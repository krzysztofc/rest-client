package pl.sda.example.restclient.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.sda.example.restclient.http.client.model.Todo;

import java.io.IOException;
import java.util.List;

@Service
public class ToDoServiceImpl implements ToDoService {

    HttpClient httpClient = HttpClientBuilder.create().build(); // wzorzec projektowy builder
    private static final Logger logger = LoggerFactory.getLogger(ToDoServiceImpl.class);

    // Te obiekty sluza do zapisywania np z ... na ...
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();


    @Override
    public Todo getToDo(long id) {
// URL pod ktorym bedzie wykonywany request HTTP
        String url = "https://jsonplaceholder.typicode.com/todos/" + id;
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse = null;

        try {
            // Wykonanie requesta httpGet
            httpResponse = httpClient.execute(httpGet);
            //PArsowanie odpowiedzi do obiektu String - JSON
            String todoAsJson = EntityUtils.toString(httpResponse.getEntity());
            logger.info("Response code: {} for request: {}", httpResponse.getStatusLine().getStatusCode(), httpGet);
            logger.info(todoAsJson);
            //Parsowanie JSON na obiekt ptzy pomocy biblioteki Jackson 2
            Todo todo = objectMapper.readValue(todoAsJson, Todo.class);
            logger.info(todo.toString());
            return todo;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Zamykanie odpowiedzi -  bardzo wazne
            HttpClientUtils.closeQuietly(httpResponse);
        }
        return null;
    }

    @Override
    public List<Todo> getTodos() {
        String url = "https://jsonplaceholder.typicode.com/todos/";
        HttpGet httpGet = new HttpGet(url);
        HttpResponse httpResponse = null;

        try{
            httpResponse = httpClient.execute(httpGet);
            String todoAsJson = EntityUtils.toString(httpResponse.getEntity());
            List<Todo> todos = objectMapper.readValue(
                    todoAsJson, objectMapper.getTypeFactory().constructCollectionType(List.class, Todo.class));
            logger.info("Response code: {} for request: {}",
                    httpResponse.getStatusLine().getStatusCode(), httpGet);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            HttpClientUtils.closeQuietly(httpResponse);
        }

        return null;
    }

    @Override
    public void addTodo(Todo todo) {
        String url = "https://jsonplaceholder.typicode.com/todos/";
        HttpPost httpPost = new HttpPost(url);
        HttpResponse httpResponse = null;
        try {
            String todoAsJson = objectWriter.writeValueAsString(todo);
            logger.info(todoAsJson);

            httpPost.setEntity(new StringEntity(todoAsJson));
            httpPost.addHeader("Content-Type", "application/json");

            httpResponse = httpClient.execute(httpPost);
            logger.info("Response code: {} for request: {}",
                    httpResponse.getStatusLine().getStatusCode(),
                    httpPost);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            HttpClientUtils.closeQuietly(httpResponse);
        }
    }

    @Override
    public void updateTodo(Todo todo) {
        String url = "https://jsonplaceholder.typicode.com/todos/"+1; //+1 czyli bedziemy robic update na id=1
        HttpPut httpPut = new HttpPut(url);
        HttpResponse httpResponse = null;

        try {
            String todoAsJson = objectWriter.writeValueAsString(todo);
            logger.info(todoAsJson);

            httpPut.setEntity(new StringEntity(todoAsJson));

            httpResponse = httpClient.execute(httpPut);
            logger.info("Response code: {} for request: {}",
                    httpResponse.getStatusLine().getStatusCode(),
                    httpPut);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            HttpClientUtils.closeQuietly(httpResponse);
        }

    }
}
