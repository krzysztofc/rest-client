package pl.sda.example.restclient.service;

import pl.sda.example.restclient.http.client.model.Todo;

import java.util.List;

public interface ToDoService {

    Todo getToDo(long id);

    List<Todo> getTodos();

    void addTodo(Todo todo);

    void updateTodo(Todo todo);


}
