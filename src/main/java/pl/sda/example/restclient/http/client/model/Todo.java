package pl.sda.example.restclient.http.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class Todo {

    private long id;
    @JsonProperty("userId")
    private long userId;
    private String title;
    private boolean completed;

    public Todo() {
    }

    public Todo(long userId, String title, boolean completed) {
        this.userId = userId;
        this.title = title;
        this.completed = completed;
    }

    public Todo(long id, long userId, String title, boolean completed) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.completed = completed;
    }
}
